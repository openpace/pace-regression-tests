1) Copy TestHarnessStartup.xml to your Titan install directory, default is "C:\Program Files\Open Pace\Pace Client\"
2) Using the GUI or Oxygen modify the path to match where you checked out the regression test files.  The entry looks like the following:
    <TestHarnessFileInfo>
      <UseDefault>true</UseDefault>
      <TestHarnessFile>C:\Users\jsmith\Documents\RegressionTests\VVBatch.btth</TestHarnessFile>
    </TestHarnessFileInfo>
3) Manually or automatically (using the NT Scheduler), open the Titan.xls workbook.  
4) Tests will run and when complete the applicaiton will close Excel.  
5) You will find any error files in the same directory as the <TestHarnessFile>, and the log in the Titan install directory.